﻿using System;
using KTDS.Server.Entity;
using IBatisNet.DataMapper;
using System.Collections;

namespace KTDS.Server.Dac
{
    public class HospitalSvcDac : IDisposable
    {
        public virtual void Dispose()
        {
        }
        /// <summary>
        /// 로그인
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public UserVO Login(ISqlMapper mapper, Hashtable param)
        {
            return mapper.QueryForObject("Login", param) as UserVO;
        }
        
        /// <summary>
        /// 사용자 상세 정보 조회
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public UserVO GetUserInfo(ISqlMapper mapper, Hashtable param)
        {
            return mapper.QueryForObject("GET_USER_INFO", param) as UserVO;
        }

    }
}
