' IIS_Log_Clear.vbs 
'지난 로그 파일을 삭제하는 VB스크립트
Option Explicit

' 삭제할 사이트 로그 파일들이 있는 폴더명을 저장할 변수
Dim strRootPath

' 폴더명 저장 변수에 실제 폴더명 문자열을 저장.
strRootPath = "\Logs\"

' WScript.echo strRootPath - 폴더명을 Alert창으로 띄움. 개발용으로 사용하고, 주석처리
Const nDays = 90    ' 90이란 값을 nDays 에 저장 (상수)

Dim wFSO
Set wFSO = CreateObject("scripting.FileSystemObject") ' 웹서버의 파일 시스템에 액세스할 수 있는 객체 변수 생성

Dim wFolder
Set wFolder = wFSO.GetFolder(strRootPath) ' strRootPath에 해당하는 폴더 정보를 가지는 객체 생성

Dim wFile

'wFolder 폴더 내에 있는 파일 삭제 루틴 (루프)
For Each wFile In wFolder.Files ' wFolder 내에 있는 파일들을 루프를 통해 순차적으로 접근
    If Int( Now() - wFile.DateLastModified ) >= nDays Then ' (오늘날짜 - 해당 파일의 최근 수정 일자)가 90일보다 크면
        wFile.Delete ' 파일 삭제
    End If
Next
