﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Web;

namespace KTDS.Server.Web.Utility
{
    public class WebUtility
    {
        /// <summary>
        /// Post방식의 파라미터를 전달하여 응답을 구하는 함수, 반환 값으로 string response를 리턴 한다.
        /// </summary>
        /// <param name="getURL">요청 대상 Url</param>
        /// <param name="lstPostData">Post 요청 여부</param>
        /// <param name="postEncode">Post parameter의 Encode</param>
        /// <param name="responseEncode">Response의 Encode</param>
        /// <param name="timeOutSec">요청 Timeout 설정 값(초)</param>
        /// <param name="statusCode">처리 결과 값</param>
        /// <returns></returns>
        public static string GetResponseByPostRequest(string getURL, Dictionary<string, string> lstPostData
            , string postEncode, string responseEncode, int timeOutSec, out HttpStatusCode statusCode, bool bRevalidateCache = false, string[] files = null)
        {
            if (getURL.Substring(0, 5).ToLower().Equals("https"))
            {

                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                {
                    return true; // **** Always accept
                };

            }

            Uri address = new Uri(getURL);

            //
            // Create the web request   
            //
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            request.UserAgent = "Mozilla/4.0";

            if (bRevalidateCache)
            {
                HttpRequestCachePolicy cachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.Revalidate);
                request.CachePolicy = cachePolicy;
            }

            //
            //TimeOut 설정
            //
            request.Timeout = timeOutSec * 1000;

            ///
            /// Expect100Continue 무시
            ///
            request.ServicePoint.Expect100Continue = false;

            //
            // Post 할 Data가 있는 경우 처리
            //
            if ((lstPostData != null) && (lstPostData.Count != 0))
            {
                if (files != null && files.Length > 0)
                {
                    lstPostData.Add("id", "TTR");
                    lstPostData.Add("btn-submit-photo", "Upload");
                }

                SetPostParam(lstPostData, postEncode, request, files);

            }


            string resultOfRequest = "";

            HttpStatusCode stateCD = HttpStatusCode.OK;

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    stateCD = response.StatusCode;
                    StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(responseEncode));

                    resultOfRequest = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    stateCD = ((HttpWebResponse)ex.Response).StatusCode;
                }
                else
                {
                    stateCD = HttpStatusCode.BadRequest;
                }

                if (ex.InnerException != null)
                    resultOfRequest = ex.InnerException.Message;
                else
                    resultOfRequest = ex.Message;
            }
            catch (Exception ex)
            {
                stateCD = HttpStatusCode.InternalServerError;

                if (ex.InnerException != null)
                    resultOfRequest = ex.InnerException.Message;
                else
                    resultOfRequest = ex.Message;
            }

            statusCode = stateCD;

            return resultOfRequest;
        }

        /// <summary>
        /// HttpWebRequest에 대한 Post param을 설정 한다.
        /// </summary>
        /// <param name="lstPostData">Post param</param>
        /// <param name="postEncode">Post param의 Encode형식</param>
        /// <param name="request">HttpWebRequest객체</param>
        private static void SetPostParam(Dictionary<string, string> lstPostData, string postEncode, HttpWebRequest request, string[] files)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // Set type to POST   
            request.Method = "POST";
            if (files != null && files.Length > 0)
            {
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                request.KeepAlive = true;
                Stream memStream = new System.IO.MemoryStream();

                var boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
                                                                        boundary + "\r\n");
                var endBoundaryBytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
                                                                            boundary + "--");


                string formdataTemplate = "\r\n--" + boundary +
                                            "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

                if (lstPostData != null && lstPostData.Count > 0)
                {
                    foreach (string key in lstPostData.Keys)
                    {
                        string formitem = string.Format(formdataTemplate, key, lstPostData[key]);
                        byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                        memStream.Write(formitembytes, 0, formitembytes.Length);
                    }
                }

                string headerTemplate =
                    "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                    "Content-Type: application/octet-stream\r\n\r\n";

                for (int i = 0; i < files.Length; i++)
                {
                    memStream.Write(boundarybytes, 0, boundarybytes.Length);
                    var header = string.Format(headerTemplate, "ktdsFileUp", files[i]);
                    var headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                    memStream.Write(headerbytes, 0, headerbytes.Length);

                    using (var fileStream = new FileStream(files[i], FileMode.Open, FileAccess.Read))
                    {
                        var buffer = new byte[1024];
                        var bytesRead = 0;
                        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }

                memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
                request.ContentLength = memStream.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    memStream.Position = 0;
                    byte[] tempBuffer = new byte[memStream.Length];
                    memStream.Read(tempBuffer, 0, tempBuffer.Length);
                    memStream.Close();
                    requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                }
            }
            else
            {
                request.ContentType = "application/x-www-form-urlencoded";
                StringBuilder data = new StringBuilder();

                foreach (string postDataName in lstPostData.Keys)
                {
                    if (data.Length != 0)
                    {
                        data.Append("&");
                    }

                    data.Append(String.Format("{0}={1}", postDataName, HttpUtility.UrlEncode(lstPostData[postDataName], Encoding.GetEncoding(postEncode))));
                }


                ////
                //// Create a byte array of the data we want to send   
                ////
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(data.ToString());


                ////
                //// Set the content length in the request headers   
                ////
                request.ContentLength = byteData.Length;

                ////
                //// Write data   
                ////
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
            }


        }

        /// <summary>
        /// 특정 문자열 내에 존재 하는 Java script로 추측 되는 Text를 제거 하는 함수
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetScriptRemoveStr(string str)
        {
            string[] arrStr = {
                    "onerror", "onblur", "onchange", "onclick", "onfocus", "onkeydown",
                    "onkeypress", "onkeyup", "onmousedown", "onmousemove", "onmousemove",
                    "onmouseout", "onmouseover", "onmouseup", "onselect", "onselectstart",
                    "onload"
            };

            System.Text.RegularExpressions.Regex[] regx = {
                new System.Text.RegularExpressions.Regex(@"(\<script(.|\n)*?\>)"),
                new System.Text.RegularExpressions.Regex(@"(\<SCRIPT(.|\n)*?\>)"),
                new System.Text.RegularExpressions.Regex(@"(\</script\>)"),
                new System.Text.RegularExpressions.Regex(@"(\</SCRIPT\>)"),
                new System.Text.RegularExpressions.Regex(@"[j|J][a|A][v|V][a|A][s|S][c|C][r|R][i|i][p|p][t|T]")
                //new System.Text.RegularExpressions.Regex(@"<\s*[i|I][n|N][p|P][u|U][t|T]")
            };

            str = regx[0].Replace(str, "<scri-pt><![CDATA[");
            str = regx[1].Replace(str, "<SCRI-PT><![CDATA[");
            str = regx[2].Replace(str, "]]></scri-pt>");
            str = regx[3].Replace(str, "]]></SCRI-PT>");
            str = regx[4].Replace(str, "JAVA-SCRIPT");
            //str = regx[3].Replace(str, "&lt;IN-PUT");

            //ArrayList pList = new ArrayList();
            List<string> pList = new List<string>();

            for (int i = 0; i < arrStr.Length; i++)
            {
                char[] arrChr = arrStr[i].ToCharArray();
                string pStr = "";

                for (int j = 0; j < arrChr.Length; j++)
                {
                    pStr += String.Format("[{0}|{1}]", arrChr[j].ToString().ToLower(), arrChr[j].ToString().ToUpper());
                }

                pList.Add(pStr);
            }

            foreach (string strPattern in pList)
            {
                System.Text.RegularExpressions.Regex newRegx = new System.Text.RegularExpressions.Regex(strPattern);
                string strMatch = newRegx.Match(str).ToString();
                str = newRegx.Replace(str, "EventReplaced-" + strMatch);
            }

            return GetUTF8String(str);
        }


        /// <summary>
        /// 특정 문자열 내에 존재 하는 Java script로 추측 되는 Text를 제거 하는 함수(제목용)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetScriptRemoveStrForTitle(string str)
        {
            string[] arrStr = {
                    "onerror", "onblur", "onchange", "onclick", "onfocus", "onkeydown",
                    "onkeypress", "onkeyup", "onmousedown", "onmousemove", "onmousemove",
                    "onmouseout", "onmouseover", "onmouseup", "onselect", "onselectstart",
                    "onload"
            };

            System.Text.RegularExpressions.Regex[] regx = {
                new System.Text.RegularExpressions.Regex(@"(\<script(.|\n)*?\>)"),
                new System.Text.RegularExpressions.Regex(@"(\<SCRIPT(.|\n)*?\>)"),
                new System.Text.RegularExpressions.Regex(@"(\</script\>)"),
                new System.Text.RegularExpressions.Regex(@"(\</SCRIPT\>)"),
                new System.Text.RegularExpressions.Regex(@"[j|J][a|A][v|V][a|A][s|S][c|C][r|R][i|i][p|p][t|T]"),
                new System.Text.RegularExpressions.Regex(@"<"),
                new System.Text.RegularExpressions.Regex(@">")
                //new System.Text.RegularExpressions.Regex(@"<\s*[i|I][n|N][p|P][u|U][t|T]")
            };

            str = regx[0].Replace(str, "<scri-pt><![CDATA[");
            str = regx[1].Replace(str, "<SCRI-PT><![CDATA[");
            str = regx[2].Replace(str, "]]></scri-pt>");
            str = regx[3].Replace(str, "]]></SCRI-PT>");
            str = regx[4].Replace(str, "JAVA-SCRIPT");
            str = regx[5].Replace(str, "&lt;");
            str = regx[6].Replace(str, "&gt;");
            //str = regx[3].Replace(str, "&lt;IN-PUT");

            //ArrayList pList = new ArrayList();
            List<string> pList = new List<string>();

            for (int i = 0; i < arrStr.Length; i++)
            {
                char[] arrChr = arrStr[i].ToCharArray();
                string pStr = "";

                for (int j = 0; j < arrChr.Length; j++)
                {
                    pStr += String.Format("[{0}|{1}]", arrChr[j].ToString().ToLower(), arrChr[j].ToString().ToUpper());
                }

                pList.Add(pStr);
            }

            foreach (string strPattern in pList)
            {
                System.Text.RegularExpressions.Regex newRegx = new System.Text.RegularExpressions.Regex(strPattern);
                string strMatch = newRegx.Match(str).ToString();
                str = newRegx.Replace(str, "EventReplaced-" + strMatch);
            }

            return GetUTF8String(str);
        }

        /// <summary>
        /// 특정 문자열 내에 존재 하는 '&lt' '&gt' 로 추측 되는 Text를 '<' '>' 복구 하는 함수(제목 수정용)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetScriptReplaceStrForTitle(string str)
        {
            System.Text.RegularExpressions.Regex[] regx = {
                new System.Text.RegularExpressions.Regex(@"&lt;"),
                new System.Text.RegularExpressions.Regex(@"&gt;")
            };

            str = regx[0].Replace(str, "<");
            str = regx[1].Replace(str, ">");

            return GetUTF8String(str);
        }

        /// <summary>
        /// Link 버튼에 오류를 Script오류를 유발 할 만한 Text제거
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetScriptRemoveStrForLinkButton(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            str = str.Replace("&", "&#38;");
            str = str.Replace("'", "&#39;");
            str = str.Replace("(", "&#40;");
            str = str.Replace(")", "&#41;");
            str = str.Replace("\"", "&#34;");
            str = str.Replace("<", "&#60;");
            str = str.Replace("=", "&#61;");
            str = str.Replace(">", "&#62;");


            return GetUTF8String(str);
        }

        /// <summary>
        /// 입력스트링을 UTF-8로 변환해 리턴.
        /// </summary>
        /// <param name="str">입력스트링</param>
        public static string GetUTF8String(string str)
        {
            byte[] arrbyte = System.Text.Encoding.UTF8.GetBytes(str);

            System.Text.Decoder dc = System.Text.Encoding.UTF8.GetDecoder();

            int charscount = dc.GetCharCount(arrbyte, 0, arrbyte.Length);

            char[] chars = new char[charscount];

            int charsDecodedCount = System.Text.Encoding.UTF8.GetChars(arrbyte, 0, arrbyte.Length, chars, 0);

            System.Text.StringBuilder sb = new System.Text.StringBuilder(charsDecodedCount);

            foreach (char c in chars)
            {
                sb.Append(c.ToString());
            }

            return sb.ToString();
        }


        #region [ DextDownload ]


        /// <summary>
        /// 문자열을 Base64로 인코딩 한 문자열을 돌려준다.
        /// </summary>
        /// <param name="sRawText">원본 문자열</param>
        /// <returns>Base64로 인코딩된 문자열</returns>
        public static string EncodeToBase64(string sRawText)
        {
            string sResult = String.Empty;

            if (!String.IsNullOrEmpty(sRawText))
            {
                Encoding UTF8Encodeing = Encoding.UTF8;
                byte[] aryString = UTF8Encodeing.GetBytes(sRawText);
                sResult = Convert.ToBase64String(aryString);
            }

            return sResult;
        }


        /// <summary>
        /// 문자열을 Base64로 인코딩 한 문자열을 돌려준다.
        /// </summary>
        /// <param name="sRawText">원본 문자열</param>
        /// <returns>Base64로 인코딩된 문자열</returns>
        public static string DecodeFromBase64(string sEncodedText)
        {
            string sResult = String.Empty;

            if (!String.IsNullOrEmpty(sEncodedText))
            {
                Encoding UTF8Encodeing = Encoding.UTF8;
                byte[] arrayToByte = Convert.FromBase64String(sEncodedText);
                sResult = UTF8Encodeing.GetString(arrayToByte, 0, arrayToByte.Length);
            }

            return sResult;
        }
        #endregion


    }

}
