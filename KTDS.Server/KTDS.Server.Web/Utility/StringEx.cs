﻿using System.Collections.Generic;
using KTDS.Server.Data;

namespace KTDS.Server.Web.Utility
{
    public class StringEx
    {
        public static string GetDictionaryValue(Dictionary<string, string> paramList, string key)
        {
            string resultValue = string.Empty;

            if (paramList.ContainsKey(key))
            {
                resultValue = paramList[key];
            }
            else
            {
                LogHelper.LoggingHandler.Error(string.Format("지정한 키가 사전에 없습니다. [{0}]", key));
            }

            return resultValue;
        }
    }
}