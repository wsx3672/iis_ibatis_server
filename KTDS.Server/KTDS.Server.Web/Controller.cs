﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using KTDS.Server.Data;

namespace KTDS.Server.Web
{
    public class Controller
    {
        public static string GetMethodDescription(MethodInfo methodInfo)
        {
            string result = string.Empty;
            object[] attrList = methodInfo.GetCustomAttributes(true);
            foreach (object attr in attrList)
            {
                if (attr.GetType().Name.Equals("DescriptionAttribute"))
                {
                    result = ((System.ComponentModel.DescriptionAttribute)attr).Description;
                }
            }

            return result;
        }

        public static void ControllerSvc(HttpRequest req, HttpResponse oRes, HttpContext oApp, string serviceType)
        {
            //Logger.LoggingHandler.Info("이벤트 시작");
            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.MaxJsonLength       = int.MaxValue;
            object objResult        = null;
            string errMsg           = string.Empty;
            string errCode          = string.Empty;
            string stateCode        = string.Empty;

            string methodName       = (req.Params["methodName"] == null ? "" : req.Params["methodName"]);
            string methodDesc       = string.Empty;
            string patientCode      = (req.Params["patientCode"] == null ? "" : req.Params["patientCode"]);
            string userID           = (req.Params["userId"] == null ? "ARUM" : req.Params["userId"]);
            string paramString      = (req.Params["params"] == null ? "" : req.Params["params"]);

            string deviceType       = (req.Params["deviceType"] == null ? "" : req.Params["deviceType"]);
            string deviceIdentName  = (req.Params["deviceIdentName"] == null ? "" : req.Params["deviceIdentName"]);
            string deviceIdentIP    = (req.Params["deviceIdentIP"] == null ? "" : req.Params["deviceIdentIP"]);
            string deviceIdentMac   = (req.Params["deviceIdentMac"] == null ? "" : req.Params["deviceIdentMac"]);

            LogHelper.LoggingHandler.Debug(string.Format("patientCode : {0}", patientCode));
            LogHelper.LoggingHandler.Debug(string.Format("userID : {0}", userID));
            LogHelper.LoggingHandler.Debug(string.Format("methodName : {0}", methodName));
            LogHelper.LoggingHandler.Debug(string.Format("paramString : {0}", paramString));

            if (string.IsNullOrEmpty(methodName))
            {
                return;
            }


            try
            {
                Type type = null;

                if (serviceType.Equals("H"))
                {
                    type = typeof(HospitalSvc);
                }

                MethodInfo method = type.GetMethod(methodName);
                methodDesc = Controller.GetMethodDescription(method);

                if (serviceType.Equals("H"))
                {
                    using (HospitalSvc consentSvc = new HospitalSvc())
                    {
                        if (string.IsNullOrEmpty(paramString) || paramString.Equals("{}"))
                            objResult = method.Invoke(consentSvc, null);
                        else
                        {
                            objResult = method.Invoke(consentSvc, new object[] { paramString });
                        }
                            

                    }
                }

                stateCode = "0";

            }
            catch (TargetException ex)
            {
                errCode = "1000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.InnerException.Message);
            }
            catch (TargetInvocationException ex)
            {
                errCode = "2000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.InnerException.Message);
            }
            catch (MethodAccessException ex)
            {
                errCode = "3000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.InnerException.Message);
            }
            catch (InvalidOperationException ex)
            {
                errCode = "4000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.InnerException.Message);
            }
            catch (NotSupportedException ex)
            {
                errCode = "5000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                errCode = "6000";
                stateCode = "-1";
                errMsg = string.Format("methodName : {0} \r\n message : {1}", methodName, ex.Message);
            }
            finally
            {
                oRes.Write(jss.Serialize(GetResultData(objResult, stateCode, errCode, errMsg)));
            }
        }


        private static Dictionary<string, object> GetResultData(object rtnData, string stateCode,string errorCode, string errorMsg)
        {
            Dictionary<string, object> resultMap = new Dictionary<string, object>();
            resultMap.Add("RESULT_CODE", stateCode); // 정상이면 0, 오류면 -1
            resultMap.Add("RESULT_DATA", (rtnData == null) ? ""  : rtnData); // ARRAY JSON 
            resultMap.Add("ERROR_CODE", errorCode); // 에러코드 
            resultMap.Add("ERROR_MESSAGE", errorMsg); // 에러메시지 
            return resultMap;
        }
    }
}