﻿using IBatisNet.DataMapper;
using KTDS.Server.Entity;
using KTDS.Server.Dac;
using KTDS.Server.Data;
using System;
using System.Collections;

namespace KTDS.Server.Web
{
    public class HospitalBiz : IDisposable
    {
        private IBatisHelper _ibatisHelper;

        public HospitalBiz()
        {
            _ibatisHelper = new IBatisHelper("OracleMap.config");
        }

        /// <summary>
        /// 현재 연결된 IBatis Connection을 반환
        /// </summary>
        public ISqlMapper IbatisMapper
        {
            get
            {
                if (_ibatisHelper == null)
                {
                    return null;
                }
                else
                {
                    return _ibatisHelper.GetSqlMapper();
                }
            }
        }

        /// <summary>
        /// 현재 연결된 ISqlMapSession 을 반환
        /// </summary>
        public ISqlMapSession IbatisSession
        {
            get
            {
                if (_ibatisHelper == null)
                {
                    return null;
                }
                else
                {
                    return _ibatisHelper.GetMapSession();
                }
            }
        }
        /// <summary>
        /// 로그인
        /// </summary>
        /// <param name="userId">아이디</param>
        /// <param name="password">패스워드</param>
        /// <returns>사용자 정보</returns>
        public UserVO Login(string userId, string password, string serverCode)
        {
            Hashtable param = new Hashtable();
            param.Add("UserID", userId);
            param.Add("UserPassword", password);
            param.Add("serverCode", serverCode);
            UserVO result = null;

            using (HospitalSvcDac dac = new HospitalSvcDac())
            {
                result = dac.Login(this.IbatisMapper, param);
            }
            return result;
        }

        public void Dispose()
        {
            if (IbatisSession != null && IbatisSession.Transaction != null)
            {
                IbatisSession.CommitTransaction();
                IbatisSession.CloseConnection();
            }
            if (IbatisMapper != null)
                IbatisMapper.CloseConnection();

        }
    }
}