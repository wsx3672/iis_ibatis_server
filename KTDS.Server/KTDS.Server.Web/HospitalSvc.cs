﻿using KTDS.Server.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Script.Serialization;
using System.ComponentModel;
using KTDS.Server.Web.Utility;

namespace KTDS.Server.Web
{
    public class HospitalSvc : IDisposable
    {
        public void Dispose()
        {
        }

        private JavaScriptSerializer ser;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HospitalSvc()
        {
            ser = new JavaScriptSerializer();
            ser.MaxJsonLength = int.MaxValue;
        }
        // 시스템 구분
        [Description("서비스 시스템 구분")]
        public static string GetServiceTarget()
        {
            string serviceTarget = "DEV";

            serviceTarget = ConfigurationManager.AppSettings["REMOTE_SERVER"];

            return serviceTarget;
        }        
        /// <summary>
        /// 로그인
        /// </summary>
        /// <returns>사용자 정보</returns>
        [Description("사용자 로그인")]
        public UserVO Login(string jsonParam)
        {
            HospitalBiz hospitalBiz = new HospitalBiz();
            Dictionary<string, string> paramList = ser.Deserialize<Dictionary<string, string>>(jsonParam);

            UserVO user = null;
            try
            {
                string password = StringEx.GetDictionaryValue(paramList, "UserPassword");
                string userId = StringEx.GetDictionaryValue(paramList, "UserID");
                string serverCode  = ConfigurationManager.AppSettings[GetServiceTarget() + "_ServerCode"]; 
                string paramLog = "";
                paramLog += "UserID[" + StringEx.GetDictionaryValue(paramList, "UserID") + "]";
                paramLog += "UserPassword[" + StringEx.GetDictionaryValue(paramList, "UserPassword") + "]";
                paramLog += "serverCode[" + serverCode + "]";
                KTDS.Server.Data.LogHelper.LoggingHandler.Debug(string.Format("KTDS Server Login[{0}] param : {1}", "UserLogin", paramLog));
                user = hospitalBiz.Login(userId, password, serverCode);
            }
            catch (Exception ex)
            {
                if (hospitalBiz != null && hospitalBiz.IbatisSession != null && hospitalBiz.IbatisSession.Transaction != null)
                {
                    hospitalBiz.IbatisSession.RollBackTransaction();
                    hospitalBiz.IbatisSession.CloseConnection();
                }
                KTDS.Server.Data.LogHelper.LoggingHandler.Error(string.Format("KTDS Server Login[{0}] error: {1}", "", ex.Message));
                throw ex;
            }
            finally
            {
                hospitalBiz.Dispose();
            }
            return user;
        }

    }
}
