﻿namespace KTDS.Server.Data
{
    using log4net;
    using log4net.Config;
    public class LogHelper
    {
        private static readonly ILog _logger = LogManager.GetLogger("KTDS");
        private static readonly ILog _loggerForiBatis = LogManager.GetLogger("IBatisNet.DataMapper.Commands.IPreparedCommand");

        public static void FinalLogging()
        {
            LogManager.Shutdown();
        }

        public static void InitLogging()
        {
            XmlConfigurator.Configure();
        }

        public static ILog LoggingHandler =>
            _logger;

        public static ILog LoggerForiBatis =>
            _loggerForiBatis;
    }
}

