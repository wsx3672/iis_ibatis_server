﻿namespace KTDS.Server.Data
{
    using IBatisNet.DataMapper;
    using IBatisNet.DataMapper.Configuration;
    using System;
    using System.Data;
    using System.Web;
    using System.Xml;

    public class IBatisHelper : IDisposable
    {
        private ISqlMapper oConn;
        private ISqlMapSession session;
        private string _providerPath;

        public IBatisHelper(string sProvidersName)
        {
            string str;
            this.oConn = null;
            this.session = null;
            this.ProviderPath = str = HttpContext.Current.Server.MapPath("~/Conf/") + sProvidersName;
            this.GetConnect(str);
        }

        public virtual void Dispose()
        {
        }

        private void GetConnect(string sProvidersPath)
        {
            if ((this.session == null) || (this.session.Connection.State != ConnectionState.Open))
            {
                DomSqlMapBuilder builder = new DomSqlMapBuilder();
                try
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(sProvidersPath);
                    this.oConn = builder.Configure(document);
                    if (this.oConn == null)
                    {
                        throw new Exception("oConn 이 Null 입니다.", new Exception("oConn 이 Null 입니다."));
                    }
                    this.session = this.oConn.BeginTransaction();
                    if (this.session == null)
                    {
                        throw new Exception("session 이 Null 입니다.", new Exception("session 이 Null 입니다."));
                    }
                    this.session.OpenConnection();
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
        }

        public ISqlMapSession GetMapSession() => 
            this.session;

        public ISqlMapper GetSqlMapper() => 
            this.oConn;

        public string ProviderPath
        {
            get => 
                this._providerPath;
            set => 
                this._providerPath = value;
        }
    }
}

