﻿using System;
using System.ComponentModel;

namespace KTDS.Server.Entity
{
    [Serializable]
    public class UserVO
    {
        public UserVO()
        {
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(this))
                property.SetValue(this, "");
        }

        public string UserID { get; set; }
        public string UserPassword { get; set; }
        public string UserName { get; set; }
    }
}
